from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views import View
from django_tables2.config import RequestConfig

from polls.models import Poll
from polls.tables import PollsTable


class IndexView(View):
    template_name = 'index.html'

    def get(self, request):
        if request.user.is_authenticated():
            return redirect('users:home')
        return render(request, self.template_name)


class UserHomePage(View):
    template_name = 'users/user_homepage.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request):
        actual_polls = PollsTable(Poll.objects.filter(end_date__gte=datetime.now()).all())
        ended_polls = PollsTable(Poll.objects.filter(end_date__lte=datetime.now()).all())
        RequestConfig(request).configure(actual_polls)
        RequestConfig(request).configure(ended_polls)

        return render(request, self.template_name, {'actual_polls': actual_polls,
                                                    'ended_polls': ended_polls})