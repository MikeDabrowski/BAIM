from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class Poll(models.Model):
    end_date = models.DateTimeField(null=False)
    title = models.CharField(max_length=1024, null=False)

    def __str__(self):
        return self.title

    def is_actual(self):
        return self.end_date >= timezone.now()


class PollOption(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE, related_name='polloptions')
    name = models.CharField(max_length=512)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    def add_vote(self):
        self.votes += 1


class UserParticipation(models.Model):
    poll = models.ForeignKey(Poll)
    user = models.ForeignKey(User)

