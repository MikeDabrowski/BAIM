from django import forms

from polls.models import Poll, PollOption


class PollForm(forms.ModelForm):
    poll_options = forms.ModelChoiceField(queryset=PollOption.objects.all(), widget=forms.RadioSelect(), initial=0)

    class Meta:
        model = Poll
        fields = ()

    def __init__(self, *args, **kwargs):
        poll = kwargs.get('instance')
        super(PollForm, self).__init__(*args, **kwargs)
        if not poll:
            raise ValueError('You must send Poll object to this form.')
        else:
            self.fields['poll_options'].queryset = poll.polloptions.all()
            self.poll_question = poll.title
            self.poll_expiration_date = poll.end_date

    def save(self, commit=True):
        poll = super().save(commit=False)
        if commit:
            poll.save()
            chosen_option = self.cleaned_data['poll_options']
            chosen_option.add_vote()
            chosen_option.save()
        return poll
