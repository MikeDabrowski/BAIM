import django_tables2 as tables

from polls.models import Poll


class PollsTable(tables.Table):
    poll_url = tables.TemplateColumn('<a href="{% url \'polls:user_poll_vote\' record.id %}">'
                                     '<button>Go to the poll</button></a>', orderable=False)

    class Meta:
        model = Poll
        template = 'django_tables2/bootstrap.html'
        sequence = ('id', 'title', 'end_date')
