from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views import View

from polls.form import PollForm
from polls.models import Poll, UserParticipation


@method_decorator(login_required, name='dispatch')
class PollVoteView(View):
    template_name = 'polls/user_poll_vote.html'
    form_class = PollForm

    def get_queryset(self):
        return Poll.objects.prefetch_related('polloptions').get(pk=self.kwargs.get('pk'))

    def dispatch(self, request, *args, **kwargs):
        poll = self.get_queryset()
        if not poll.is_actual():
            return redirect('polls:user_poll_details', pk=(self.kwargs.get('pk')))
        if UserParticipation.objects.filter(user=request.user, poll=poll).exists():
            return render(request, 'polls/user_poll_already_voted.html', {'poll': poll})

        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        poll = self.get_queryset()
        form = PollForm(instance=poll)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = PollForm(request.POST, instance=self.get_queryset())
        if form.is_valid():
            form.save()
            # annotate that user has already participate to this poll
            UserParticipation(user=request.user, poll=self.get_queryset()).save()
            return render(request, 'polls/user_poll_success_vote.html')
        else:
            return render(request, self.template_name, {'form': form})


@method_decorator(login_required, name='dispatch')
class PollDetailsView(View):
    template_name = 'polls/user_poll_details.html'

    def get_queryset(self):
        return Poll.objects.prefetch_related('polloptions').get(pk=self.kwargs.get('pk'))

    def get(self, request, *args, **kwargs):
        poll = self.get_queryset()
        if not request.user.is_superuser and poll.is_actual():
            return redirect('polls:user_poll_vote', pk=(self.kwargs.get('pk')))
        return render(request, self.template_name, {'poll': poll})

