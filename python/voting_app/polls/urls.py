from django.conf.urls import url

from polls import views

app_name = 'polls'
urlpatterns = [
    url(r'^vote/(?P<pk>\d+)/$', views.PollVoteView.as_view(), name='user_poll_vote'),
    url(r'^details/(?P<pk>\d+)/$', views.PollDetailsView.as_view(), name='user_poll_details'),
]
