from django.contrib import admin
from django.urls import reverse

from polls.models import Poll, PollOption


class PollOptionsInline(admin.StackedInline):
    model = PollOption
    readonly_fields = ('votes', )
    extra = 0
    min_num = 2
    max_num = 6


class PollAdmin(admin.ModelAdmin):
    list_display = ('title', 'end_date', 'options', 'poll_votes', 'is_actual')
    inlines = [PollOptionsInline]

    def options(self, obj):
        return list(obj.polloptions.all())

    def poll_votes(self, obj):
        return '<a href={}>see voting results</a>'.format(reverse('polls:user_poll_details', kwargs={'pk': obj.id}))
    poll_votes.allow_tags = True


admin.site.register(Poll, PollAdmin)
