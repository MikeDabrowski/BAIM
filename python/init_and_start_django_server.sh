#!/usr/bin/env bash

set -e # stop execution when error occurs

python manage.py migrate
python manage.py collectstatic --noinput
python manage.py loaddata initial_data.json # load default admin user data

uwsgi --ini config/uwsgi_settings.ini
