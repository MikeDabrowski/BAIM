#!/bin/bash
# wait-for-postgres.sh

host="$1"
shift
cmd="sh init_and_start_django_server.sh"

# make forever loop. And break when mysql will be available.
while true; do
    curl -f "$host" &> /dev/null
    if [ "$?" = 0 ]; then
        break
    fi
    echo "Waiting for MySql docker. Host: $host"
    sleep 1
done

echo "Mysql is up - executing command"
exec $cmd