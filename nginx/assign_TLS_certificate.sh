#!/usr/bin/env bash

set -e

# TLS certificate assigning - will be valid only on production server.
certbot --non-interactive --authenticator standalone --installer nginx --agree-tos --redirect --email drapek39@wp.pl -d drapiewski.pl
service nginx reload
nginx