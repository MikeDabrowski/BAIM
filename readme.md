# TODO 

 - [x] Rewrite it to python-social-auth (newer version of this library)
 - [x] Delete github authorization
 - [x] Move admin login page from default /admin/ urls to more secure...
 - [x] change the logout page and add logout option to user panel (maybe to base.html)
 - [x] Handle exception: "This google-oauth2 account is already in use." When user already logged in try one more time to log in.
 - [x] Do something about  the problem with voting from different accounts.
 - [x] Write custom middleware (or pipeline) to get user data which is needed. Less is more.
 - [x] Add proper database connection (not sqlite but mysql from docker container)
 - [x] Join apache and python containers (maybe just mount python dir in apache dir?)
 - [ ] Configure modsecurity
 - [ ] Configure docker permissions
 - [ ] Add fixture with admin user

# Uruchomienie
1. [Docker-Compose](https://docs.docker.com/compose/install/)
2. edytować maszyne virtualną dodając do niej katalog projektu (full_project_path to ścieżka do katalogu BAIM)
```bash
docker-machine stop

cd <vbox location>
vboxmanage sharedfolder add default --name "docker-apache2" --hostpath "<full_project_path>" --automount

docker-machine start  
```
3. Z katalogu głównego wykonać `docker-compose up`. Kiedy zakonczy się instalacja należy zatrzymac obraz CTRL+C 
i uruchomić go `docker-compose start` 

Każda zmiana konfiguracji dockerowo apachowej prawdopodobnie wymaga przebudowania. Zmiany samego django raczej nie, 
pliki sie same odświeżają, ale może trzeba przeładowac aplikację (z konsoli ubuntu)

## Przydatne komendy
- `docker ps`
- `docker image -a`
- `docker rmi <imagename_or_id>`
- `docker-compose start/stop`
- `docker-compose build`
- `docker exec -it <nazwa obrazu> bash` - dostęp do konsoli w ubuntu
- [https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes](https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes)

BASED ON:
See [blog post](http://ramkulkarni.com/blog/docker-project-for-python3-djaongo-and-apache2-setup/) and [this video](https://youtu.be/OtZmCBR7J-k)
