#!/bin/bash
# Zakończenia lini muszą byc unixowe (LF)

echo "Creating schemas"
echo "CREATE DATABASE IF NOT EXISTS \`vote_system\`; " | "${mysql[@]}"

echo "Creating users"
echo "CREATE USER '$MYSQL_PC_USER'@'%' IDENTIFIED BY '$MYSQL_PC_PASSWORD' ;" | "${mysql[@]}"

echo "Granting privileges to $MYSQL_USER"
echo "GRANT ALL ON \`vote_system\`.* TO '$MYSQL_PC_USER'@'%' WITH GRANT OPTION;" | "${mysql[@]}"

echo "Done"

